﻿using System;

namespace GcdTask
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Calculates GCD of two integers from [-int.MaxValue;int.MaxValue]  by the Euclidean algorithm.
        /// </summary>
        /// <param name="a">First integer.</param>
        /// <param name="b">Second integer.</param>
        /// <returns>The GCD value.</returns>
        /// <exception cref="ArgumentException">Thrown when all numbers are 0 at the same time.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when one or two numbers are int.MinValue.</exception>
        public static int FindGcd(int a, int b)
        {
            int c;

            if (a < 0)
            {
                a *= -1;
            }
            
            if (b < 0)
            {
                b *= -1;
            }

            if (a == 0 && b == 0)
            {
                throw new ArgumentException("bad one", nameof(a));
            }
            else if (a == int.MinValue || b == int.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(a), "bad one");
            }
            else
            {
                while (b != 0)
                {
                    c = b;
                    b = a % b;
                    a = c;
                }

                return a; 
            }
        }
    }
}
